from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout




def user_registration(request):
	form = UserCreationForm()

	if request.method == "GET":
		return render(request, 'users/registration.html', {'form':form, 'user':request.user})
	else: 
		username = request.POST['username']
		password1 = request.POST['password1']
		password2 = request.POST['password2']
		if password1 == password2:

			try:
				user = User.objects.create_user(
					username=username,
					password=password1,
				)
				user.save()
				login(request, user)
			except:
				print('User creation error')
		else:
			return render(request, 'users/registration.html', {'form':form, 'user':request.user, 'error':'Passwords didn\'t match'})

		return redirect('homepage')

def user_login(request):
	form = AuthenticationForm()

	if request.method == "GET":
		return render(request, 'users/login.html', {'form':form, 'user':request.user})
	else: 
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(request, username=username, password=password)

		if user is not None:
			login(request, user)
			return redirect('homepage')
		else:
			return render(request, 'users/login.html', {'form':form, 'user':request.user, 'error':"Username or password is wrong"})

def user_logout(request):
	logout(request)
	return redirect('login')