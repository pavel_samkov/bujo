from django.db import models
from django.contrib.auth.models import User


class Task(models.Model):
	"""Model to representation Tasks"""		
	ACTIVE = 'active'
	CANCEL = 'cancel'
	DONE = 'done'

	owner = models.ForeignKey(User, on_delete=models.CASCADE)						#many to one relationship, deletes all tasks if we delete related user 
	title = models.CharField(max_length=150)										#taks title 
	description = models.TextField(null=True, blank=True)							#task description
	deadline = models.DateField(null=True, blank=True)								#deadline time is optional 
	status = models.CharField(max_length=6, default=ACTIVE)

	def __str__(self):																	#name our object in admin panel 
		return self.title
