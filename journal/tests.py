from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.http import HttpRequest


from datetime import date, timedelta


from .models import Task
from .services import date_transformer, is_leap_year, get_days_in_month, \
					filter_tasks


class TaskModelTests(TestCase):

	def test_task_status_when_created(self):
		"""task.status is ACTIVE when it created"""
		user = User.objects.create_user('test', 'test@email.com', 'test')					#test user created for test application

		task = Task(owner=user,
					title='test_task')
		self.assertIs(task.status, Task.ACTIVE)

class IsLeapYearTests(TestCase):
	def test_leap_1992(self):
		self.assertTrue(is_leap_year(1992))
	def test_leap_1584(self):
		self.assertTrue(is_leap_year(1584))
	def test_not_leap_2022(self):
		self.assertFalse(is_leap_year(2022))
	def test_not_leap_1700(self):
		self.assertFalse(is_leap_year(1700))


class GetDaysInMonthTests(TestCase):
	def test_january(self):
		self.assertEqual(31, get_days_in_month(1, 2022))
	def test_leap_year_february(self):
		self.assertEqual(29, get_days_in_month(2, 2000))
	def test_not_leap_year_february(self):
		self.assertEqual(28, get_days_in_month(2, 2022))
	def test_march(self):
		self.assertEqual(31, get_days_in_month(3, 2022))
	def test_april(self):
		self.assertEqual(30, get_days_in_month(4, 2022))	
	def test_may(self):
		self.assertEqual(31, get_days_in_month(5, 2022))
	def test_june(self):
		self.assertEqual(30, get_days_in_month(6, 2022))
	def test_july(self):
		self.assertEqual(31, get_days_in_month(7, 2022))
	def test_august(self):
		self.assertEqual(31, get_days_in_month(8, 2022))	
	def test_september(self):
		self.assertEqual(30, get_days_in_month(9, 2022))
	def test_october(self):
		self.assertEqual(31, get_days_in_month(10, 20220))
	def test_november(self):
		self.assertEqual(30, get_days_in_month(11, 20221))
	def test_december(self):
		self.assertEqual(31, get_days_in_month(12, 20222))	


class DateTransformerTests(TestCase):
	def test_today(self):
		test_today = date.fromisoformat('2000-01-12')
		test_date = '2000-01-12'
		self.assertEqual(test_date, date_transformer('today', today=test_today))

	def test_tomorrow_leap_year(self):
		test_today = date.fromisoformat('2000-02-28')
		test_date = '2000-02-29'
		self.assertEqual(test_date, date_transformer('tomorrow', today=test_today))

	def test_tomorrow_not_leap_year(self):	
		test_today = date.fromisoformat('2022-02-28')
		test_date = '2022-03-01'
		self.assertEqual(test_date, date_transformer('tomorrow', today=test_today))

	def test_tomorrow_new_year(self):	
		test_today = date.fromisoformat('2022-12-31')
		test_date = '2023-01-01'
		self.assertEqual(test_date, date_transformer('tomorrow', today=test_today))

	def test_this_week_sunday(self):	
		test_today = date.fromisoformat('2022-08-21')
		test_date = '2022-08-21'
		self.assertEqual(test_date, date_transformer('this-week', today=test_today))

	def test_this_week_monday(self):	
		test_today = date.fromisoformat('2022-08-15')
		test_date = '2022-08-21'
		self.assertEqual(test_date, date_transformer('this-week', today=test_today))

	def test_this_month_first_day(self):	
		test_today = date.fromisoformat('2022-08-01')
		test_date = '2022-08-31'
		self.assertEqual(test_date, date_transformer('this-month', today=test_today))

	def test_this_month_last_day(self):	
		test_today = date.fromisoformat('2022-08-31')
		test_date = '2022-08-31'
		self.assertEqual(test_date, date_transformer('this-month', today=test_today))

	def test_this_month_leap_year(self):	
		test_today = date.fromisoformat('2000-02-01')
		test_date = '2000-02-29'
		self.assertEqual(test_date, date_transformer('this-month', today=test_today))

	def test_this_year_first_day(self):	
		test_today = date.fromisoformat('2022-01-01')
		test_date = '2022-12-31'
		self.assertEqual(test_date, date_transformer('this-year', today=test_today))

	def test_this_year_last_day(self):	
		test_today = date.fromisoformat('2022-12-31')
		test_date = '2022-12-31'
		self.assertEqual(test_date, date_transformer('this-year', today=test_today))

	def test_this_year_middle_of_the_year(self):	
		test_today = date.fromisoformat('2022-07-22')
		test_date = '2022-12-31'
		self.assertEqual(test_date, date_transformer('this-year', today=test_today))

	def test_custom_date(self):	
		test_date = '1234-01-02'
		self.assertEqual(test_date, date_transformer('custom-date', custom_date='1234-01-02'))

	def test_custom_date(self):	
		test_date = '2022-09-10'
		self.assertEqual(test_date, date_transformer('custom-date', custom_date='2022-09-10'))

	def test_no_deadline(self):	
		test_today = None
		self.assertEqual(None, date_transformer('no-deadline'))


class FilterTasksTests(TestCase):

	def test_with_no_params(self):
		c = Client()
		user = User.objects.create_user('test', 'test@email.com', 'test')
		logged_in = c.login(username='test', password='test')	

		test_tasks = Task(
			title='test',
			description='test',
			deadline=date_transformer('tomorrow'),
			owner=user)

		test_tasks.save()

		request = HttpRequest()
		request.user = logged_in
		request.content_params = {'deadline': 'tomorrow'}

		tasks = filter_tasks(request)

		self.assertEqual(tasks, test_tasks)


"""	def test_only_by_date_today(self):
		c = Client()
		response = c.get('/homepage/', {'deadline_choice': 'today'})

	def test_only_by_date_tomorrow(self):
		c = Client()
		response = c.get('/homepage/', {'deadline_choice': 'tomorrow'})

	def test_only_by_date_this_week(self):
		c = Client()
		response = c.get('/homepage/', {'deadline_choice': 'this-week'})

	def test_only_by_date_this_month(self):
		c = Client()
		response = c.get('/homepage/', {'deadline_choice': 'this-month'})

	def test_only_by_date_this_year(self):
		c = Client()
		response = c.get('/homepage/', {'deadline_choice': 'this-year'})

	def test_only_by_date_no_deadline(self):
		c = Client()
		response = c.get('/homepage/', {'deadline_choice': 'no-deadline'})

	def test_only_by_date_custom_date(self):
		c = Client()
		response = c.get('/homepage/', {'deadline_choice': 'custom-date', 
										'deadline': '2022-05-22'})

	def test_only_by_date_today_and_one_status(self):
		c = Client()
		response = c.get('/homepage/', {'deadline_choice': 'today', 
										'status': 'active'})

	def test_only_by_date_today_and_two_statuses(self):
		c = Client()
		response = c.get('/homepage/', {'deadline_choice': 'today', 
										'status': 'active',
										'status': 'done'})

	def test_only_by_date_today_and_all_statuses(self):
		c = Client()
		response = c.get('/homepage/', {'deadline_choice': 'today', 
										'status': 'active',
										'status': 'done',
										'status': 'cancel'})
"""
		