from django.urls import path 
from . import views


urlpatterns = [
	path('home/', views.get_homepage, name='homepage'),
	path('create-task/', views.create_task_view, name='create_task'),
	path('edit-task/<int:id>/', views.edit_task_view, name='edit_task'),
	path('cancel_task/<int:id>/', views.cancel_task_view, name='cancel_task'),
	path('done_task/<int:id>/', views.done_task_view, name='done_task'),
	path('notes', views.notes_view, name='notes'),
	path('calendar', views.calendar_view, name='calendar'),
	path('tracker', views.tracker_view, name='tracker'),
]