from datetime import date, timedelta
from django.db.models import Q


from .models import Task
from .forms import TaskForm


def is_leap_year(year):
    """ Function to check if year is leap or not
    ---------------------------------------------------------------------------
    Params:

    year - checked year
    ---------------------------------------------------------------------------
    Functionality:

    Step 1.If year is evenly divisible by 4 -> step 2, otherwise -> step 5
    Step 2. If year is evenly divisible by 100 -> step 3, otherwise -> step 4
    Step 3. If year is evenly divisible by 400 -> step 4, otherwise -> step 5
    Step 4. It is a leap year
    Step 5. It is NOT a leap year
    """
    return False if year % 4 != 0 \
        else True if year % 100 != 0 \
        else True if year % 400 == 0 \
        else False


def get_days_in_month(month, year):
    """Function to getting days in month, get month from 1 to 12,
        where 1 is January and 12 is December
    ---------------------------------------------------------------------------
    Params:

    month - checked month
    year - if we check second month (February == 2), we have to check year
    is leap or not
    ---------------------------------------------------------------------------
    Functionality:

    if index of the month is in thirty_days list -> returns 30
    if index of the month is in thirty_one_days list -> returns 31
    if index of the month is 2, call is_leap_year(year) -> returns 28 or 29
    """

    thirty_days = [4, 6, 9, 11]
    thirty_one_days = [1, 3, 5, 7, 8, 10, 12]

    if month in thirty_days:
        return 30
    elif month == 2:
        if is_leap_year(year):
            return 29
        return 28
    else:
        return 31


def filter_tasks(request):
    """Function to getting values from database and filter them
    ---------------------------------------------------------------------------
    Params:

    request - for getting parameters (status, deadline_choice, deadline)

    ---------------------------------------------------------------------------
    Functionality:

    Because we can filter by 3 statuses in same time, we get list of status.
    Choice and date are single values, so we set them to 0 for start, and try
    to get this values from request.

    We always get choice param except one case(after creation task) CASE 1.

    Case 1, after creating task, we get 0 parameters -> get all today's tasks.
    Case 2, filtering only by date, getting all tasks with chosen date.
    Case 3, filtering by date and one status, getting all tasks with,
        chosen date and chosen status.
    Case 4, filtering by date and two statuses, getting all tasks with chosen
        date and chosen statuses.
    Case 5, filtering by date and three statuses, getting all tasks with chosen
        date and chosen statuses.

    Function returns QueryDict with tasks
    """
   
    status_list = request.GET.getlist('status')

    try:
        choice = request.GET.getlist('deadline_choice')[0]
        date = request.GET.getlist('deadline')[0]
    except IndexError:
        choice, date = 0, 0

    if len(status_list) == 0 and not choice:
        tasks = Task.objects.filter(owner=request.user,
                                    deadline=date_transformer('today'))

    elif len(status_list) == 0:
        tasks = Task.objects.filter(owner=request.user,
                                    deadline=date_transformer(choice, date))

    elif len(status_list) == 1:
        tasks = Task.objects.filter(owner=request.user,
                                    status=status_list[0],
                                    deadline=date_transformer(choice, date))
    elif len(status_list) == 2:
        tasks = Task.objects.filter(Q(status=status_list[0]) |
                                    Q(status=status_list[1]),
                                    deadline=date_transformer(choice, date),
                                    owner=request.user)

    elif len(status_list) == 3:
        tasks = Task.objects.filter(
                                    Q(status=status_list[0]) |
                                    Q(status=status_list[1]) |
                                    Q(status=status_list[2]),
                                    deadline=date_transformer(choice, date),
                                    owner=request.user)

    return tasks


def date_transformer(choice, custom_date=None, today=date.today()):
    """Function to transform string choices to dates
    ---------------------------------------------------------------------------
    Params:

    choice - string choice
    custom_date - is needed if choice == custom-date
    today - always use date.today(), but for testing it is used as parameter
    ---------------------------------------------------------------------------
    Functionality:

    no-deadline -> set and returns NULL
    custom-date -> set result and returns custom date
    this-year -> set result and returns last day of current year
    this-month -> set result and returns last day of current month
    this-week -> set result and returns last day(Sunday) of current week
    tomorrow -> set result and returns next day after today
    today -> set result and returns today's date

    function returns string value in 'YYYY-MM-DD' format
    date format is checking in form in create_task view
    """

    result = ''

    if choice == 'no-deadline':
        return None

    elif choice == 'custom-date':
        result = custom_date

    elif choice == 'this-year':
        result = str(today.year) + '-12-31'

    elif choice == 'this-month':
        result = today + timedelta(days=(get_days_in_month(
                                        today.month, today.year) - today.day))

    elif choice == 'this-week':
        result = today + timedelta(days=(7 - today.isoweekday()))

    elif choice == 'tomorrow':
        result = today + timedelta(days=1)

    elif choice == 'today':
        result = today

    return str(result)
