from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


from .models import Task
from .services import date_transformer, filter_tasks
from .forms import TaskForm, FilterForm


@login_required
def get_homepage(request):
    form = FilterForm()
    content = {'tasks': filter_tasks(request), 'form': form}
    return render(request, 'journal/index.html', content)


@login_required
def create_task_view(request):
    if request.method == 'GET':
        form = TaskForm()
        return render(request, 'journal/create_task.html', {'form': form})

    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            try:
                task = Task(
                    title=form.cleaned_data['title'],
                    description=form.cleaned_data['description'],
                    deadline=date_transformer(
                                    form.cleaned_data['deadline_choice'],
                                    form.cleaned_data['deadline']),
                    owner=request.user)
                task.save()
                return redirect('homepage')
            except ValidationError as e:
                return render(request,
                              'journal/create_task.html',
                              {'form': form,
                               'errors': 'Something went wrong, try again'})


@login_required
def edit_task_view(request, id):
    task = get_object_or_404(Task, owner=request.user, pk=id)
    if request.method == 'GET':
        form = TaskForm(initial={'title': task.title,
                                 'description': task.description})
        return render(request, 'journal/edit_task.html', {'form': form})
    else:
        form = TaskForm(request.POST)
        if form.is_valid():
            task.title = form.cleaned_data['title']
            task.description = form.cleaned_data['description']
            task.deadline = date_transformer(
                                        form.cleaned_data['deadline_choice'],
                                        form.cleaned_data['deadline'])
        task.save()
        return redirect('homepage')


@login_required
def cancel_task_view(request, id):
    if request.method == 'GET':
        task = Task.objects.get(owner=request.user, pk=id)
        task.status = Task.CANCEL
        task.save()
        return redirect('homepage')


@login_required
def done_task_view(request, id):
    if request.method == 'GET':
        task = Task.objects.get(owner=request.user, pk=id)
        task.status = Task.DONE
        task.save()
        return redirect('homepage')


@login_required
def notes_view(request):
    return render(request, 'journal/notes.html')


@login_required
def calendar_view(request):
    return render(request, 'journal/calendar.html')


@login_required
def tracker_view(request):
    return render(request, 'journal/tracker.html')
