from django import forms
from django.forms import ModelForm


from .models import Task

STATUS_CHOICES = [(Task.ACTIVE, 'Active'), (Task.DONE, 'Doned'), (Task.CANCEL, 'Cancelled')]
DATE_CHOICES = [('today', 'Today'), ('tomorrow', 'Tomorrow'), 
					('this-week', 'This week'),('this-month', 'This month'),
					('this-year', 'This year'), ('no-deadline', 'No deadline'),
					('custom-date',  'Custom date')]


class TaskForm(forms.Form):
	
	title = forms.CharField(max_length=100)
	description = forms.CharField(widget=forms.Textarea)

	deadline = forms.DateField(widget=forms.widgets.DateInput(attrs={'type': 'date'}), required=False)

	deadline_choice = forms.ChoiceField(label="Deadline", 
										widget=forms.Select(), 
										choices=DATE_CHOICES)


class FilterForm(forms.Form):
	status = forms.ChoiceField(label="Status", 
									widget=forms.CheckboxSelectMultiple(), 
									choices=STATUS_CHOICES)	

	deadline = forms.DateField(widget=forms.widgets.DateInput(attrs={'type': 'date'}), required=False)

	deadline_choice = forms.ChoiceField(label="Deadline", 
										widget=forms.Select(), 
										choices=DATE_CHOICES)